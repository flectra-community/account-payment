# Copyright 2015-2017 Eficent Business and IT Consulting Services S.L.
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "Account Due List Aging Comment",
    "version": "2.0.1.0.0",
    "category": "Generic Modules/Payment",
    "author": "Eficent," "Odoo Community Association (OCA),",
    "website": "https://gitlab.com/flectra-community/account-payment",
    "license": "AGPL-3",
    "depends": [
        "account_due_list",
    ],
    "data": [
        "views/payment_view.xml",
    ],
    "installable": True,
}
