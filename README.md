# Flectra Community / account-payment

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[account_cash_discount_write_off](account_cash_discount_write_off/) | 2.0.1.0.0|         Create an automatic writeoff for payment with discount on the payment        order confirmation
[account_payment_return_import](account_payment_return_import/) | 2.0.1.0.2| This module adds a generic wizard to import payment returnfile formats. Is only the base to be extended by anothermodules
[account_cash_discount_base](account_cash_discount_base/) | 2.0.1.0.0| Account Cash Discount Base
[account_due_list_days_overdue](account_due_list_days_overdue/) | 2.0.1.0.0| Payments Due list days overdue
[account_payment_multi_deduction](account_payment_multi_deduction/) | 2.0.1.1.0| Payment Register with Multiple Deduction
[partner_aging](partner_aging/) | 2.0.1.0.2| Aging as a view - invoices and credits
[account_cash_invoice](account_cash_invoice/) | 2.0.1.2.0| Pay and receive invoices from bank statements
[account_payment_batch_process_discount](account_payment_batch_process_discount/) | 2.0.1.0.0| Discount on batch payments
[account_payment_widget_amount](account_payment_widget_amount/) | 2.0.1.0.0| Extends the payment widget to be able to choose the payment amount
[account_cash_discount_payment](account_cash_discount_payment/) | 2.0.1.0.1| Account Cash Discount Payment
[account_payment_batch_process](account_payment_batch_process/) | 2.0.1.0.0|         Account Batch Payments Processing for Customers Invoices and        Supplier Invoices    
[account_payment_credit_card](account_payment_credit_card/) | 2.0.1.0.1| Add support for credit card payments
[account_payment_term_extension](account_payment_term_extension/) | 2.0.1.0.3| Adds rounding, months, weeks and multiple payment days properties on payment term lines
[account_payment_terminal](account_payment_terminal/) | 2.0.1.0.0| This addon allows to pay invoices using payment terminal
[account_due_list](account_due_list/) | 2.0.1.0.0| List of open credits and debits, with due date
[account_payment_paired_internal_transfer](account_payment_paired_internal_transfer/) | 2.0.1.0.0| Crete internal transfers in one move.
[account_due_list_payment_mode](account_due_list_payment_mode/) | 2.0.1.0.0| Payment Due List Payment Mode
[account_due_list_aging_comment](account_due_list_aging_comment/) | 2.0.1.0.0| Account Due List Aging Comment
[account_payment_term_discount](account_payment_term_discount/) | 2.0.1.1.6| Account Payment Terms Discount
[account_payment_view_check_number](account_payment_view_check_number/) | 2.0.1.0.0| Account Payment View Check Number
[account_payment_return_import_iso20022](account_payment_return_import_iso20022/) | 2.0.1.0.0|         This addon allows to import payment returns from ISO 20022 files        like PAIN or CAMT.
[account_payment_return](account_payment_return/) | 2.0.1.0.4| Manage the return of your payments


