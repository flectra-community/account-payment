# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Payment Due List Payment Mode",
    "version": "2.0.1.0.0",
    "category": "Generic Modules/Payment",
    "author": "Obertix Free Solutions, Tecnativa, Odoo Community Association (OCA),",
    "license": "AGPL-3",
    "website": "https://gitlab.com/flectra-community/account-payment",
    "depends": ["account_payment_partner", "account_due_list"],
    "data": ["views/payment_view.xml"],
    "application": False,
    "installable": True,
}
